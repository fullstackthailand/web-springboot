package net.trueid.platform.authentication.repository;

import net.trueid.platform.authentication.entities.Shops;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopsRepository extends JpaRepository<Shops,Integer> {

}
