package net.trueid.platform.authentication.repository;

import net.trueid.platform.authentication.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsersRepository extends JpaRepository<Users,Integer>{

    Users findByEmail(String email);
    //Users save(Users users);
}
