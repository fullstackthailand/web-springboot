package net.trueid.platform.authentication.handlers;

import net.trueid.platform.authentication.exceptions.TrueIDException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = {TrueIDException.class})
    public ResponseEntity<?> handler(HttpServletRequest request, TrueIDException ex, HttpServletResponse httpServletResponse) throws IOException {

        return ResponseEntity.status(400).body(ex.getErrorResponse());
    }

}


