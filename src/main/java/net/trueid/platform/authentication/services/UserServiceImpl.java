package net.trueid.platform.authentication.services;


import net.trueid.platform.authentication.entities.Users;
import net.trueid.platform.authentication.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired private UsersRepository usersRepository;
    //@Autowired private RedisService redisService;

    @Override
    public List<Users> listAll() {
        //List<Users> users = usersRepository.findAll();
        return usersRepository.findAll();
    }

    @Override
    public Users findByEmail(Map<String,String> inputs) throws IOException {
        /*String data = redisService.get(inputs.get("email"));
        if (data.isEmpty()) {
            redisService.set(inputs.get("email"), "Hello", 0);
            return usersRepository.findByEmail(inputs.get("email"));
        } else {

            return redisService.get(inputs.get("email"));
        }*/
        return usersRepository.findByEmail(inputs.get("email"));
    }

    @Override
    public Optional<Users> findShopByUserId(Integer id) {
        return usersRepository.findById(id);
    }

    @Override
    public Users save(Users users)  {
        return usersRepository.save(users);
    }

    @Override
    public void deleteById(Integer id) {
        usersRepository.deleteById(id);
    }
}
