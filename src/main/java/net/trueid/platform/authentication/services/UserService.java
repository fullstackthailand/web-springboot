package net.trueid.platform.authentication.services;

import net.trueid.platform.authentication.entities.Users;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UserService {

    List<Users> listAll();
    Users findByEmail(Map<String,String> inputs) throws IOException;
    Optional<Users> findShopByUserId(Integer id);
    Users save(Users users);
    void deleteById(Integer id);
}
