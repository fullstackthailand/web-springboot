package net.trueid.platform.authentication.controllers.api;

import net.trueid.platform.authentication.entities.Shops;
import net.trueid.platform.authentication.entities.Users;
//import net.trueid.platform.authentication.repository.UsersRepository;
import net.trueid.platform.authentication.repository.ShopsRepository;
import net.trueid.platform.authentication.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/login")
public class LoginController {

    //@Autowired private UsersRepository usersRepository;
    @Autowired private UserService userService;
    @Autowired private ShopsRepository shopsRepository;

    @RequestMapping(value = "/email", method = { RequestMethod.GET})
    public Users index(@RequestParam Map<String,String> allRequestRarams, HttpServletRequest httpRequest) throws Exception {
        System.out.println("###### All Params ######");
        System.out.println(allRequestRarams);

        System.out.println("###### All httpRequest ######");
        System.out.println(httpRequest);

        return userService.findByEmail(allRequestRarams);
    }

    @RequestMapping(value = "/lists", method = { RequestMethod.GET})
    public List<Users> lists(@RequestParam Map<String,String> allRequestRarams, HttpServletRequest httpRequest) throws Exception {
        /*List<Users> users = usersRepository.findAll();
        return users;*/
        return userService.listAll();
    }

    @RequestMapping(value = "/shops", method = {RequestMethod.GET})
    public List<Shops> shops(@RequestParam Map<String,String> inputs) throws Exception {
        return shopsRepository.findAll();
    }

    @RequestMapping(value = "/shop/owner", method = {RequestMethod.GET})
    public Optional<Shops> shopOwner(@RequestParam Map<String,String> inputs) throws Exception {
        Optional<Shops> shops = shopsRepository.findById(Integer.parseInt(inputs.get("shopId")));
        //String shopOwner = shops.get().getName();

        return shops;
    }

    @RequestMapping(value = "/users", method = {RequestMethod.GET})
    public Optional<Users> users(@RequestParam Map<String,String> inputs) throws Exception {
        Optional<Users> users = userService.findShopByUserId(Integer.parseInt(inputs.get("userId")));
        //String shopOwner = shops.get().getName();

        return users;
    }

}