package net.trueid.platform.authentication.controllers;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class ApiController {

    @RequestMapping(value = "/api",  method = { RequestMethod.GET})
    public @ResponseBody
    String demo(@RequestParam Map<String,String> allRequestParams, HttpServletRequest httpRequest) throws Exception {
        return "OK";
    }
}