package net.trueid.platform.authentication.controllers;

import net.trueid.platform.authentication.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import net.trueid.platform.authentication.repository.UsersRepository;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/home")
public class HomeController {
    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("")
    public ModelAndView demo(@RequestParam Map<String,String> inputs) {
        Optional<Users> user = usersRepository.findById(1);
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("item", user.get());
        mav.addObject("item2", user.get());
        return mav;
    }

    @GetMapping(value = "/user")
    @ResponseBody
    public Users list(@RequestParam Map<String,String> inputs) throws Exception {
        /*System.out.println("###### All Params ######");
        System.out.println(inputs);*/

        Optional<Users> user = usersRepository.findById(1);

        return user.get();
    }

}
