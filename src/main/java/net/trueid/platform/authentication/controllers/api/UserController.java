package net.trueid.platform.authentication.controllers.api;

import net.trueid.platform.authentication.entities.Users;
import net.trueid.platform.authentication.exceptions.TrueIDException;
import net.trueid.platform.authentication.repository.UsersRepository;
import net.trueid.platform.authentication.services.UserService;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
//@PropertySource("classpath:foo.properties")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UsersRepository usersRepository;
    /*@Value("${foo}")
    String foo;*/

    private static final Logger logger = LogManager.getLogger(UserController.class);


    //@RequestMapping(value = "", method = { RequestMethod.GET, RequestMethod.POST})
    @GetMapping(value = "")
    public List<Users> index(@RequestParam Map<String,String> inputs) throws Exception {
        /*System.out.println("###### All Params ######");
        System.out.println(inputs);*/
        logger.info("Example log from {}");

        return userService.listAll();
    }

    //@Cacheable(value = "user", key = "#id")
    @GetMapping(value = "/{id}")
    public Users getById(@PathVariable String id, @RequestParam Map<String,String> inputs) throws Exception {
        Optional<Users> user = usersRepository.findById(Integer.parseInt(id));
        logger.info("Get User by ID No Caching : " + id);

        return user.get();
    }

    @Cacheable(value = "user", key = "#email", unless = "#result == null")
    @GetMapping(value = "/email")
    public Users email(@RequestParam Map<String,String> inputs, @RequestParam(value = "email") String email) throws Exception {
        /*System.out.println("###### All Params ######");
        System.out.println(inputs);*/
        logger.info("Example log from {}: " + email);

        return userService.findByEmail(inputs);
    }

    @PostMapping(value = "")
    public Users store(@Valid @RequestParam Map<String,String> inputs) throws Exception {
        /*try {
            if (inputs.get("name") == null || inputs.get("name").equals("")  ) {
                throw new TrueIDException("400", "Required name");
            }

            Users user = new Users();
            user.setName(inputs.get("name"));
            user.setSurname(inputs.get("surname"));
            user.setEmail(inputs.get("email"));
            user.setPassword(inputs.get("password"));
            user.setAge(Integer.parseInt(inputs.get("age")));
            user.setAddress(inputs.get("address"));
            user.setCity(inputs.get("city"));
            user.setMobile(inputs.get("mobile"));
            //usersRepository.save(user);
            //userService.save(user);
            //return user;
            return usersRepository.save(user);
        }catch (TrueIDException ex) {
            throw ex;
        }catch (Exception ex2) {
            ex2.printStackTrace();
        }

        return null;*/

        Users user = new Users();
        user.setName(inputs.get("name"));
        user.setSurname(inputs.get("surname"));
        user.setEmail(inputs.get("email"));
        user.setPassword(inputs.get("password"));
        user.setAge(Integer.parseInt(inputs.get("age")));
        user.setAddress(inputs.get("address"));
        user.setCity(inputs.get("city"));
        user.setMobile(inputs.get("mobile"));
        //usersRepository.save(user);
        //userService.save(user);
        //return user;
        return usersRepository.save(user);
    }

    @CachePut(value = "user", key = "#id")
    @PutMapping(value = "/{id}")
    public Users update(@PathVariable String id, @RequestParam Map<String,String> inputs) throws Exception {
        System.out.println("###### Update All Params ######");
        System.out.println(inputs);
        System.out.println(id);

        Optional<Users> user = usersRepository.findById(Integer.parseInt(id));
        user.get().setName(inputs.get("name"));
        user.get().setSurname(inputs.get("surname"));
        user.get().setEmail(inputs.get("email"));
        user.get().setPassword(inputs.get("password"));
        user.get().setAge(Integer.parseInt(inputs.get("age")));
        user.get().setAddress(inputs.get("address"));
        user.get().setCity(inputs.get("city"));
        user.get().setMobile(inputs.get("mobile"));
        //usersRepository.save(user.get());
        userService.save(user.get());

        return user.get();
    }

    @CacheEvict(value = "user", key = "#id")
    @DeleteMapping(value = "/{id}")
    public String destroy(@PathVariable String id) throws Exception {
        //System.out.println("###### Delete All Params ######");
        //usersRepository.deleteById(Integer.parseInt(id));
        userService.deleteById(Integer.parseInt(id));

        return "Delete";
    }

    @GetMapping(value = "/debug")
    public String debug(@RequestParam Map<String,String> inputs) throws Exception {
        /*String foo = System.getProperty("foo");
        String version = System.getProperty("version");
        String ifoo = System.getProperty("info.foo");
        String iversion = System.getProperty("info.version");
        return "XXX=" + foo + version + "=XXXX="+ ifoo + iversion+ "=XXXX";*/

        String xx = System.getProperty("xx");
        System.out.println("###### All Params ######");
        System.out.println(xx);

        //return "XXX=" + foo + "=XXXX";
        return "xxx";
    }

}
