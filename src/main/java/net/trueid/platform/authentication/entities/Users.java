package net.trueid.platform.authentication.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name="users")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Users implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    @NotEmpty(message = "Name is a required field")
    @Size(min=2,  message="Name should have atleast 2 characters")
    @Size(max = 100,  message="Name should have not over 5 characters")
    private String name;

    @Column(name = "surname")
    @NotEmpty(message = "Surname is a required field")
    @Size(min=2,  message="Surname should have atleast 2 characters")
    @Size(max = 100,  message="Surname should have not over 5 characters")
    private String surname;

    @Column(name = "email")
    @NotEmpty(message = "Email is a required field")
    @Email(message = "Email is a email pattern")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "age")
    private Integer age;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "active")
    private Integer active;

    @Column(name = "api_token")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String api_token;

    //@JsonProperty("remember_token")
    @Column(name = "remember_token")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rememberToken;

    //@JsonProperty("created_at")
    @Column(name = "created_at")
    private Timestamp createdAt;

    //@JsonProperty("updated_at")
    @Column(name = "updated_at")
    private Timestamp updatedAt;

    @OneToOne(mappedBy = "users", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Shops shops;

    /*@OneToOne(mappedBy="users")
    private Shops shops;*/

}
