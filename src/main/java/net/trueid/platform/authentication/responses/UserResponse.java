package net.trueid.platform.authentication.responses;

import lombok.Data;

@Data
public class UserResponse {
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private Integer age;
    private String address;
    private String city;
    private String mobile;
    private Integer active;
    private String api_token;
    private String rememberToken;
}
