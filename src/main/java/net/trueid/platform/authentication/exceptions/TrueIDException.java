package net.trueid.platform.authentication.exceptions;

import net.trueid.platform.authentication.responses.ErrorResponse;

public class TrueIDException extends Exception {
    private ErrorResponse errorResponse;

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }

    public TrueIDException(String code, String message) {
        this.errorResponse = new ErrorResponse();
        this.errorResponse.setCode(code);
        this.errorResponse.setMessage(message);
    }

}

